package lesson2.homework;

public class task3 {
    public static void main(String[] args) {
        String str = " Hi loo  k      p  o  w  ";
        System.out.println(str.replaceAll("( +)", " ").trim()); //самый простой способ

        String str1 = "      Java  Hel      lo   World";
        int indexOfNewWord = 0;
        StringBuilder builder = new StringBuilder();
        String str2 = str1.trim();
        for (int i = 0; i < str2.length(); i++) {
            if ((str2.charAt(i) == ' ')) {
                if (str2.charAt(i + 1) == ' ') {
                    builder.append(str2.substring(indexOfNewWord, i));
                    builder.append("");
                    indexOfNewWord = i + 1;
                }
            }
        }
        builder.append(str2.substring(indexOfNewWord, str2.length()));

        System.out.println(builder.toString());
    }
}
