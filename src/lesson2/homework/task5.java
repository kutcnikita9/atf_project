package lesson2.homework;

public class task5 {
    public static void main(String[] args) {
        String str = "1 раз, 3 раза, снова 3 раза и наконец, 1 и 3 раза";
        int one = 0;
        int two = 0;
        int tri = 0;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '1') {
                one = one + 1;
            } else if (str.charAt(i) == '2') {
                two = two + 1;
            } else if (str.charAt(i) == '3') {
                tri = tri + 1;
            }
        }
        System.out.println("Колличество 1: " + one);
        System.out.println("Колличество 2: " + two);
        System.out.println("Колличество 3: " + tri);
    }
}
