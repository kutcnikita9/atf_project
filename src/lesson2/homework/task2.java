package lesson2.homework;

public class task2 {
    public static void main(String[] args) {
        int n = 19;
        int count = 0;
        for (int i = 1; i < n; i++) {
            if ((i < n) && (i % 2 != 0) && (i % 3 != 0) && (i % 5 != 0))
                count = count + 1;
        }   // если по задании имелось ввиду ВКЛЮЧАЯ n, то необходимо поменять знак "<" на "<=" в цикле for и условии "i < n"
        System.out.println(count);
    }
}
