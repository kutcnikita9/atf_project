package lesson3.homework;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class task3 {
    public static void main(String[] args) {
        List<String> lst = new ArrayList<>();

        lst.add("Test");
        lst.add("Test");
        lst.add("Hello");
        lst.add("World");
        lst.add("World");
        lst.add("WORLD");

        Map<String, Integer> mapa = new HashMap<>();

        for (String string : lst) {
            if (mapa.containsKey(string)) {
                mapa.put(string, mapa.get(string) + 1);
            } else {
                mapa.put(string, 1);
            }
        }

        System.out.println(mapa);
    }

}

