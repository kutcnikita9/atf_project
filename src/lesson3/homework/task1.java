package lesson3.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class task1 {
    public static void main(String[] args) {
        List<Integer> list1 = Arrays.asList(1, 3);
        List<Integer> list2 = Arrays.asList(2);
        List<Integer> result = new ArrayList<>();
        double pointer1 = 0.0;
        double pointer2 = 0.0;
        double m = 0.0;
        while (pointer1 < list1.size() && pointer2 < list2.size()) {
            if (list1.get((int) pointer1) < list2.get((int) pointer2)) {
                result.add(list1.get((int) pointer1));
                pointer1++;
            } else {
                result.add(list2.get((int) pointer2));
                pointer2++;
            }
        }
        if (pointer1 < list1.size()) {
            while (pointer1 < list1.size()) {
                result.add(list1.get((int) pointer1));
                pointer1++;
            }
        }
        if (pointer2 < list2.size()) {
            while (pointer2 < list2.size()) {
                result.add(list2.get((int) pointer2));
                pointer2++;
            }
        }
        if ((pointer1 + pointer2) % 2 != 0) {
            m = result.get((int) ((pointer1 + pointer2) / 2));
        } else {
            m = result.get(result.size() / 2 - 1) + result.get(result.size() / 2);
            m = m / 2;
        }
        System.out.println(m);
    }
}
