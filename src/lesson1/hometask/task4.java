package lesson1.hometask;

public class task4 {
    public static void main(String[] args) {
        byte max_byte = Byte.MAX_VALUE;
        byte min_byte = Byte.MIN_VALUE;
        short min_short = Short.MIN_VALUE;
        short max_short = Short.MAX_VALUE;
        int max_int = Integer.MAX_VALUE;
        int min_int = Integer.MIN_VALUE;
        long max_long = Long.MAX_VALUE;
        long min_long = Long.MIN_VALUE;
        char max_char = Character.MAX_VALUE;
        char min_char = Character.MIN_VALUE;
        System.out.println("Диапазон типа byte от " + min_byte + " до " + max_byte);
        System.out.println("Диапазон типа short от " + min_short + " до " + max_short);
        System.out.println("Диапазон типа int от " + min_int + " до " + max_int);
        System.out.println("Диапазон типа long от " + min_long + " до " + max_long);
        System.out.println("Диапазон типа char от " + min_char + " до " + max_char);
    }
}

