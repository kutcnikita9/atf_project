package lesson4.task1;

public class Person {
    private static final String Default_Name = "XXX";
    private static final int Default_Age = 18;

    public void talk(String name) {
        System.out.println("Такой-то " + name + " говорит");
    }

    public void move(String name) {
        System.out.println("Такой-то " + name + " двигается");
    }
}

