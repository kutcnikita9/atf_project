package lesson4.task2;

public class Bayer {
        private String surname;
        private String name;
        private String patronymic;
        private String theAddress;
        private String creditCardNumber;
        private int BankAccountNumber;

        public Bayer(String surname, String name, String patronymic, String theAddress, String creditCardNumber, int BankAccountNumber) {
            this.surname = surname;
            this.name = name;
            this.patronymic = patronymic;
            this.theAddress = theAddress;
            this.creditCardNumber = creditCardNumber;
            this.BankAccountNumber = BankAccountNumber;
        }

        public int getSurNameLength() {
            return this.surname.length();
        }

        public String getSurname() {
            return surname;
        }

        public String getName() {
            return name;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public String getCreditCardNumber() {
            return this.creditCardNumber;
        }

        public String getTheAddress() {
            return theAddress;
        }
    }

