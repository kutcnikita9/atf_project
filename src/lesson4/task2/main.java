package lesson4.task2;

public class main {
    public static void main(String[] args) {
        Bayer[] buyer = new Bayer[5];
        buyer[0] = new Bayer("Иванов", "Иван", "Евгеньевич", "Москва", "12345", 12345);
        buyer[1] = new Bayer("Петров", "Петр", "Евгеньевич", "Санкт-Питербург", "1234", 4321);
        buyer[2] = new Bayer("Сидров", "Сидр", "Евгеньевич", "Вологда", "54321", 4321);
        buyer[3] = new Bayer("Александрова", "Александра", "Евгеньевич", "Ташкент", "1234", 4321);
        buyer[4] = new Bayer("Антонов", "Антон", "Евгеньевич", "Нальчик", "5555", 4321);
        int a = 0;
        for (int i = 0; i < 4; i++) {
            if (buyer[a].getSurNameLength() < buyer[i + 1].getSurNameLength()) {
                a = i + 1;
            }
        }
        System.out.println("Самое длинное имя у покупателя: " + buyer[a].getName());
        for (int i = 0; i < 5; i++) {
            if (buyer[i].getCreditCardNumber().charAt(0) == '5') {
                System.out.println("Номер кредитки начинается с 5 у покупателей с адресом: " + buyer[i].getTheAddress());
            }
        }
        for (int i = 0; i < 5; i++) {
            if (buyer[i].getPatronymic().equals("Евгеньевич")) {
                System.out.println("Покупатель: " + buyer[i].getSurname() + " " + buyer[i].getName() + " " + buyer[i].getPatronymic());
            }
        }
    }
}

