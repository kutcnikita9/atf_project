package lesson5.task2;

public class Book implements Printable {
    private String nameOfBook;
    private int yearPublicationBook;

    public Book(String nameOfBook, int yearPublicationBook) {
        this.nameOfBook = nameOfBook;
        this.yearPublicationBook = yearPublicationBook;
    }

    @Override
    public void print() {
        System.out.println("Название книги '" + this.nameOfBook + "'");
    }

    public static void printBooks(Printable[] printable) {
        for (Printable value : printable) {
            Book book = (Book) value;
            System.out.println("Название книги '" + book.nameOfBook + "' год издания " + book.yearPublicationBook);
        }
    }
}
