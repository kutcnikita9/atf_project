package lesson5.task2;

public class Magazine implements Printable {
    private String nameOfMagazine;
    private int yearPublicationMagazine;

    public Magazine(String nameOfMagazine, int yearPublicationMagazine) {
        this.nameOfMagazine = nameOfMagazine;
        this.yearPublicationMagazine = yearPublicationMagazine;
    }

    @Override
    public void print() {
        System.out.println("Название журнала '" + this.nameOfMagazine + "'");
    }

    public static void printMagazines(Printable[] printable) {
        for (Printable value : printable) {
            Magazine magazine = (Magazine) value;
            System.out.println("Название журнала '" + magazine.nameOfMagazine + "' год издания " + magazine.yearPublicationMagazine);
        }
    }
}
