package lesson5.task2;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        Printable[] printables = {new Book("Война и мир", 1992), new Book("Мертвые души", 2006), new Book("Отцы и дети", 1976), new Magazine("Добрые советы", 2020), new Magazine("Караван историй", 2021), new Magazine("Работница", 2022)};
        int magazineSize = 0;
        int bookSize = 0;
        for (Printable printable : printables) {
            if (printable instanceof Magazine) {
                printable.print();
                magazineSize++;
            } else {
                if (printable instanceof Book) {
                    printable.print();
                    bookSize++;
                }
            }
        }
        Printable[] magazine = new Printable[magazineSize];
        Printable[] book = new Printable[bookSize];
        int indexForMagazines = 0;
        int indexForBook = 0;
        for (Printable printable : printables) {
            if (printable instanceof Magazine) {
                magazine[indexForMagazines] = printable;
                indexForMagazines++;
            } else {
                if (printable instanceof Book) {
                    book[indexForBook] = printable;
                    indexForBook++;
                }
            }
        }
        System.out.println();
        Magazine.printMagazines(magazine);
        Book.printBooks(book);
    }
}
