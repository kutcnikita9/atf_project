package lesson5.task4;

public class Student {
    protected static final int STUDENT_GRADE_EXCELLENT =5;
    protected String firstName;
    protected String lastName;
    protected String group;
    protected double averageMark;

    public Student(String firstName, String lastName, String group, double averageMark) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
        this.averageMark = averageMark;
    }

    public void getScholarship() {
        if (averageMark == STUDENT_GRADE_EXCELLENT) {
            System.out.println("У студента с фамилией: " + this.firstName + ", стипендия 100 гривен");
        } else {
            System.out.println("У студента с фамилией: " + this.firstName + ", стипендия 80 грвен");
        }
    }
}
