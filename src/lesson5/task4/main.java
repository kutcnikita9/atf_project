package lesson5.task4;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Aspirant("Иванов", "Иван", "44-1", 3.4));
        students.add(new Aspirant("Петров", "Перт", "46-2", 4.1));
        students.add(new Student("Кумов", "Кум", "44-6", 2.8));
        students.add(new Student("Александров", "Александровна", "43-3", 5));
        for (Student student : students) {
            student.getScholarship();
        }
    }
}
