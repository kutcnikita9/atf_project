package lesson5.task4;

public class Aspirant extends Student{
    protected String firsname;
    protected String lastname;
    protected String group;
    protected double averageMark;

    public Aspirant(String firstName, String lastName, String group, double averageMark) {
        super(firstName, lastName, group, averageMark);
    }

    @Override
    public void getScholarship() {
        if (super.averageMark == STUDENT_GRADE_EXCELLENT) {
            System.out.println("У аспиранта c фамилией: " + this.firstName + ", стипендия 200 гривен");
        } else {
            System.out.println("У аспиранта с фамилией: " + this.firstName + ", стипендия 180 грвен");
        }
    }

}
