package lesson5.task1;

public class Skirt extends Cloth implements Women {
    public Skirt(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }

    @Override
    public void dressingManInClassClothes() {
    }
    @Override
    public void dressingWomanInClassClothes() {
    }
    @Override
    public void womenDressing() {
        System.out.println("Женщину одели в юбку " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}
