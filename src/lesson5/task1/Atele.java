package lesson5.task1;

public class Atele {
    public void womenDressing(Cloth cloth) {
        cloth.dressingWomanInClassClothes();
    }
    public void menDressing(Cloth cloth) {
        cloth.dressingManInClassClothes();
    }
}
