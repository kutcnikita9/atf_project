package lesson5.task1;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        ArrayList<Cloth> cloths = new ArrayList<>();
        cloths.add(new Pants(Size.XXS, 2, "зелёного"));
        cloths.add(new Skirt(Size.L, 50, "синего"));
        cloths.add(new Tie(Size.XS, 600, "черного"));
        cloths.add(new Tshirt(Size.M, 1500, "белого"));

        Atele atelier = new Atele();
        for (Cloth clouth : cloths) {
            atelier.menDressing(clouth);
        }
        System.out.println();
        for (Cloth clouth : cloths) {
            atelier.womenDressing(clouth);
        }
    }
}
