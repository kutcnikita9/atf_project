package lesson5.task1;

public class Pants extends Cloth implements Women, Men {
    public Pants(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }

    @Override
    public void dressingManInClassClothes() {
    }
    @Override
    public void dressingWomanInClassClothes() {
    }
    @Override
    public void menDressing() {
        System.out.println("Мужчину одели в штаны " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
    @Override
    public void womenDressing() {
        System.out.println("Женщину одели в штаны " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}
