package lesson5.task1;

public  class Tie extends Cloth implements Men {
    public Tie(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }
    @Override
    public void dressingManInClassClothes() {
    }
    @Override
    public void dressingWomanInClassClothes() {
    }
    @Override
    public void menDressing() {
        System.out.println("Мужчину одели в галстук " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}
