package lesson5.task1;

public class Tshirt extends Cloth {
    public Tshirt(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }
    @Override
    public void dressingManInClassClothes() {
        System.out.println("Мужчину одели в футболку " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
    @Override
    public void dressingWomanInClassClothes() {
        System.out.println("Женщину одели в футболку " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}
