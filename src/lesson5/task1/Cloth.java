package lesson5.task1;

public abstract class Cloth {
    protected Size clothingSize;
    protected int price;
    protected String color;

    public Cloth(Size clothingSize, int price, String color) {
        this.clothingSize = clothingSize;
        this.price = price;
        this.color = color;
    }
    public abstract void dressingManInClassClothes();
    public  abstract void dressingWomanInClassClothes();
}
